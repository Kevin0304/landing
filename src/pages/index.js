import React from "react";
import ContactSection from "./../components/ContactSection";


function IndexPage(props) {
  return (
    <div>
      <div className="super-container">
        <div className="flex-container">
          <div className="first-container">
            <div className="menu-title pos-semi-bottom"><a href="#tutors">TUTORS</a></div>
          </div>
          <div className="second-container">
            <div className="logo-container"></div>
            <div className="menu-title pos-bottom"><a href="#support">SUPPORT</a></div>
          </div>
          <div className="third-container">
            <div className="menu-title pos-top"><a href="#about">ABOUT US</a></div>
            <div className="menu-title pos-semi-bottom"><a href="#download">GET THE APP</a></div>
          </div>
        </div>
      </div>

      <div className="super-container">
        <div className="info-container" id="about">
          <h1>ABOUT US</h1>

          <p className="info-content">JabCHEM is a social and educational project that aims to contribute in alleviating
          the lack of
          diverse representation in our education systems - with particular emphasis on certain fields of study
                where this lack of diversity is most prominent.<br></br>
                Tutorials are aimed at being generation tailored and makes an effort to utilise social media culture in
                the facilitation of generation relevant education.<br></br>
                Aims to contribute to the re-imaging of fields of study, to individuals who may see said topics as
                selectively inclusive of a certain image and demeanour.<br></br>
                Attempts to deviate from the 'really smart' stigma surrounding some fields of study and create an
                inclusive image of all fields.<br></br>
                JABCHEM aims to give individuals the opportunity to utilise their fields of study by providing a
                potential source of income.<br></br>
                It is the hope that JABCHEM can one day facilitate students or graduates in different parts of the
                world the opportunity to earn an income through the app.<br></br>
                JABCHEM tutorials are audio based giving the user the ability to utilise the tutorials while dealing
                with everyday life. It alleviates the immediate need for use for heavy books and allows the user to
                take advantage of the time whilst dealing with other tasks.<br></br>
                Our tutorials are based on a Q&A format - this is to allow the user to learn in the most common
                format they will be expected to prove their knowledge - in exams.
                JABCHEM consists of JABCHEM courses and Public courses.<br></br>
                Have something to teach? You can have a public course on JABCHEM.
                If you have the knowledge, we’ll do the rest.<br></br>
                We at JabCHEM want to put education in your hands. <br></br>After all, who better to decide than you?</p>
        </div>
      </div>
      <div className="super-container">
        <div className="info-container" id="support">
          <h1>SUPPORT</h1>
          <p className="info-content">
            JabCHEM is funded by a single individual however we understand if you are unable to resist the
            urge to make it rain on JabCHEM.
            Any funds raised will go towards achieving the main principles of JABCHEM<br></br>

            We'll tell you how you can support us soon!
            </p>
        </div>
      </div>
      <div className="super-container">
        <div className="info-container"
          id="tutors"
        >
          <h1>TUTORS</h1>

          <p className="info-content">
            JabCHEM will begin recruitment soon - stay tuned!
          </p>
          {/* <ContactSection
            color="white"
            size="medium"
            backgroundImage=""
            backgroundImageOpacity={1}
            title="Tutors"
            subtitle="Become a Tutor"
            buttonText="Submit Request"
            buttonColor="primary"
            buttonInverted={false}
            showNameField={true}
          /> */}
        </div>
      </div>

      <div className="super-container">
        <div className="info-container" id="download">
          <h1>GET THE APP</h1><br></br>
          <p className="coming-soon">
            COMING SOON!
            </p>
          <div class="store-logo">
          </div>
        </div>
      </div>
    </div>
  );
}

export default IndexPage;
